import { sendMsg } from '@/api/modules/ai/chatgpt';
import type { GptMessage } from '@/api/modules/ai/types';
import useConfigStore from '@/store/config/config';
import { displaySetup } from '@/views/phone/head/data';
import { conversation } from '@/views/sidebar/sidebar';
import { message } from 'ant-design-vue';
import type { AxiosError } from 'axios';
import { showSetting } from '../setting/setting';

export const msg = ref<string>('');
export const dataLoading = ref();
export const send = async (inputEl?: Ref<HTMLElement | undefined>) => {
  const configStore = useConfigStore();
  const event = window.event as MouseEvent;
  if (!msg.value) {
    return;
  }
  if (event.ctrlKey || event.altKey) {
    msg.value += '\n';
    return;
  } else {
    event.preventDefault();
  }
  dataLoading.value = true;
  let tempMsg = undefined;
  try {
    const newMsg: GptMessage = {
      role: 'user',
      content: msg.value,
    };
    conversation.value.messageList.push({
      ...newMsg,
    });

    if (configStore.$state.memory) {
      tempMsg = conversation.value.messageList;
    } else {
      tempMsg = [newMsg];
    }

    const { data } = await sendMsg({
      messages: tempMsg,
      model: configStore.$state.model,
      stream: false,
      temperature: configStore.$state.temperature,
      top_p: 0.7,
    });
    msg.value = '';

    data.choices.forEach(e => {
      conversation.value.messageList.push({
        role: e.message.role,
        content: e.message.content,
      });
    });

    nextTick(() => {
      if (!inputEl) return;
      inputEl.value?.focus();
    });
    dataLoading.value = false;
  } catch (error) {
    dataLoading.value = false;
    if (configStore.$state.memory) {
      conversation.value.messageList.pop();
    }
    const err = error as AxiosError;
    const response = err.response as any;
    if (response.data) {
      console.log(response.data.error);

      if (response.data.error.type === 'one_api_error') {
        message.warning('请填写秘钥');
        /* PC和移动分别弹出不同的设置框 */
        if (window.innerWidth < 1000) {
          displaySetup.value = true;
        } else {
          showSetting.value = true;
        }
        return;
      }
    }
    message.warn('Unknown error');
  }
};
